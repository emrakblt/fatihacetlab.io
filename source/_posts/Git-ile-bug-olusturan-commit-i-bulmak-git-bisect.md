---
title: Git ile bug olusturan commit'i bulmak - git bisect
tags: |-

  - git
  - debugging
  - git-bisect
permalink: git-bisect
id: 6
updated: '2014-11-28 20:13:22'
date: 2014-11-28 20:04:39
---

Gecen hafta calisan ama bugun calismayan bir ozellige rastladiniz ve kodda saga sola baktiniz ama nerede ve nasil patladigini bulamadiniz. `git bisect` kullanarak hangi commit ile kirildigini cok daha kolay bir sekilde bulabilirsiniz. 

`git bisect` iki commit id arasinda binary search yaparak kisa bir sure icinde size bir bug'in ortaya ciktigi commit id'sini veriyor. 

`git bisect`'i kullanmak icin oncellikle 2 commmit id'sine ihtiyacimiz var. Birincisi herseyin duzgun oldugu bir commit id'si ve ozelligimizin calismadigi commit id'si. Bu commit id'leri bulduktan sonra `git bisect`'e baslayabiliriz.

Calisan commit id'si `1111` olsun.
Calismayan commit id'si `9999` olsun.

```
git bisect start
git bisect good 1111
git bisect bad 9999
```

Bu komutlardan sonra git'e bir aralik vermis olduk. Git bizim icin binary search ile ortalama 6-8 adimda[1] bozuk olan commit id'sini verecek. Simdi gidip sayfamizi refresh edip test edelim. Eger bug'imiz hala yerinde duruyorsa `git bisect bad` bug'i tekrar edemiyorsak `git bisect good` komutunu calistiralim. Bunu yaptiktan sonra git baska bir commit id'ye checkout ederek tekrar test etmemizi isteyecek. Ayni sekilde `git bisect good` `git bisect bad` diye devam ederek sonuncu adama kadar gelelim ve butun adimlari bitirdikten sonra git bize aradigimiz commit'i kendi elleriyle teslim edecek.

Commit'imizi bulduktan sonra bisect islemini bitirmek icin `git bisect reset` demeniz yeterli olacaktir.

Ufak bir not, bisect'e baslamadan once bulundugunuz branch'in aynisindan bir branch daha acip onun uzerinde yaparsaniz hata yaptiginiz yada geri donmek istediginiz anda cok daha rahat edersiniz. Bulundugunuz branch'ten yeni bir branch daha acmak icin `git checkout -b yeni-branch` eski branch'inize donmek icin `git checkout eski-branch` demeniz yeterli olur.

[1] Commit araligina gore adim sayisi degismektedir. Cok genis aralik verirseniz daha cok adim test etmek durumunda kalirsiniz.