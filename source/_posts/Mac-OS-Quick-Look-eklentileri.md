---
title: Mac OS Quick Look eklentileri
tags: |-

  - mac
  - mac-os
permalink: mac-os-quick-look-eklentileri
id: 12
updated: '2015-06-29 01:07:28'
date: 2015-06-29 01:03:22
---

Mac OS'da bir dosya secip Space tusuna basinca cikan ufak on izleme penceresi cok kullanisli fakat Mac OS'da fazla dosya tipini desteklemiyor. Desteklenmeyen bir dosyasi secip Space tusuna basinca varsayilan pencereyi goruyorsunuz.

![http://f.acet.me/2806152356-al4qo.png](http://f.acet.me/2806152356-al4qo.png)

GitHub'daki [sindresorhus](https://github.com/sindresorhus/) denen amca [quick-look-plugins](https://github.com/sindresorhus/quick-look-plugins) diye bir repo acip Mac OS'un Quick Look ozelligini farkli dosya tipleri icin de saglamis ve cok guzel yapip cok da iyi yapmis, sagolsun.

Yukledikten sonra su sekilde guzelce kullanabiliyorsunuz.



![http://f.acet.me/2806152348-gcw5d.png](http://f.acet.me/2806152348-gcw5d.png)

![http://f.acet.me/2806152349-uqfy6.png](http://f.acet.me/2806152349-uqfy6.png)

![http://f.acet.me/2806152349-pgklb.png](http://f.acet.me/2806152349-pgklb.png)

![http://f.acet.me/2906150002-dmv8k.png](http://f.acet.me/2906150002-dmv8k.png)