---
title: Acik kaynak reklamciligi
tags: |-

  - acik-kaynak
  - github
  - gitlab
permalink: acik-kaynak-reklamciligi
id: 17
updated: '2016-11-02 00:38:44'
date: 2016-11-02 00:24:54
---

Acik kaynak projenizi hazirladiniz. Artik sira projenizin reklamini yapmak, daha cok insana ulasip daha fazla star almak ve hatta projenize disardan destek almaya geldi. Bu yazimda, bunu saglayabilmeniz icin projenizi Twitter, Hackernews, ilgili Facebook gruplari gibi sosyal mecralarda paylasmadan hemen once yapilmasi gereken onemli islerden bahsedecegim.

---

Oncelikle hemen belirtim, "haci senin GitHub'da en fazla star almis repon 10 kusur star almis sen nasil boyle bir yazi yazabiliyorsun" diye dusunenlere "sen benim dedigimi yap, yaptigimi(yapmadigimi) yapma" diyorum. Bu yazida anlatacaklarim acik kaynak projenize katki saglayarak degerini arttiracaktir. Yapilmasi gereken en oncelikli ve en onemli adimlar bu yazinin konusu olacak. Unuttugum, gozden kacirdigim, atladigim yerler olabilir ama tamamen aksini dusunen varsa cikisa bekliyorum :)

- README insanlarin projenize dair gordugu ilk sey, dolayisiyla guzel bir README bu isin olmazsa olmazi.
- Ingilizce yazilmis bir README daha genis kitlelere hitap edecektir.
- README icine projenizi anlatan ufak bir gif eklemek insanlarin yaptiginiz isi anlamasini cok daha kolaylastiracaktir.
- README icindeki bir baska onemli nokta ise renkli Build badge'leri. Projenizin daha profesyonal olmasini, gozukmesini saglayacaktir. Insanlar bir bakista en son build durumunu, hangi npm versiyonu oldugunu gorecektir.
- README'nize insanlarin projenizi test edebilecegi bir link koyarsaniz bu da cok etkili olacaktir. GitHub'in `gh-pages`'i, GitLab'in `pages`'i veya CodePen, JSFiddle bu is icin bicilmis kaftan.
- Gelistirme ortaminda kolaylik saglamak projenize katki saglamansini cok daha kolaylastiracaktir.
- Projenizi nasil kullanacaklarina dair detayli bir dokumantasyon yazin. Eger projeniz bir JavaScript library'si ise nasil kullanacaklarini gosteren kisa ve acik ornekler yazin. Eger projeniz diger insanlarin makinesine kurulmasini gereken biseyse bunu en kisa yapacaklari hale getirin. 8-10 adimli bir install sureci olan projelere katki gelmesi daha zor olacaktir. Hem kendiniz icin hem de diger insanlar icin guzel ve kolay bir gelistirme ortami saglayin.
- Projenize katkida bulunmak isteyen insanlara yol gosterin. GitHub ve GitLab'in issues kismini aktif olarak kullanin. Issue'larinizi orada listeleyin ve projenize katkida bulunmak isteyen insanlarin kolaylikla baslayacagi issue'lari `up-for-grabs`, `for-contributors` gibi label'lar kullanarak etiketleyin ki katki saglamak isteyenler bu tarz issue'lari alip direk baslayabilsinler.
- Kodunuzu temiz ve anlasilir yazmaya ozen gosterin. Kodunuzun icine aciklamalar ekleyerek anlasilir kilin ve insanlarin kodunuzu anlamasini kolaylastirin.
- Anlasilir, detayli, ornekler ile desteklenmis bir dokumantasyon yazin. GitHub, GitLab Wiki bu is yapilmis araclar. Ne kadar detayli ve basit yazarsaniz, insanlarin katki saglamasi ve alip kullanmasi o kadar kolay olacaktir.
- Eger varsa ve biliyorsaniz projeniz ile ilgili teknik uyarilari, kisitlamalari belirtin. Ornegin su library'nin su versiyonu ile calismaz, su aletin su surumunu kullanmaniz gerekir, eger soyle bir sikinti ile karsilasirsaniz su sekilde cozebilirsiniz gibisinden.
- Projenizin aklinizda gidecegi yeri gosteren bir roadmap yapmaniz cok buyuk bir arti olacaktir. Bu sayede diger insanlar sizin gelecekte yapmayi dusundugunuz ozellikler icin Merge Request gonderebilir.
- Dogru bir lisans secmek ayrica onemli. Bunun icin [ChooseaLicense](http://choosealicense.com/)'i kullanabilirsiniz.


Aklima gelenler simdilik bunlar. Bu noktada bu blog post'un cok yazidan ibaret oldugunu dusundum ve bir ornek gostermek icin GitHub'i actim. Cok yildiz almis bu pratikleri uygulamis bir repo bakarken [Adem Ilter](https://twitter.com/ademilter) ve [Fatih Kadir Akin](https://twitter.com/fkadev)'in [bricklayer](https://github.com/ademilter/bricklayer)'ina tekrar denk geldim. Soyle bir goz attim ve adamlar gercekten butun best practice'leri uygulamislar ve an itibari ile 2049 star almislar. O yuzden yerli mali bir repo'dan ornekler gosterecegim.

### README
<img src="http://f.acet.me/TtuC.png" style="max-width:700px" />


### Wiki
<img src="http://f.acet.me/upE1.png" style="max-width:700px" />


----

<blockquote class="twitter-tweet" data-lang="en"><p lang="tr" dir="ltr">Yeni Blog yazim: Acik Kaynak Reklamciligi <a href="https://t.co/ysKIwEVVhK">https://t.co/ysKIwEVVhK</a></p>&mdash; Fatih Acet (@fatihacet) <a href="https://twitter.com/fatihacet/status/793567275383156739">November 1, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

---
