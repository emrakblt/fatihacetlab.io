---
title: 4 Mart'da Bilkent'teyiz
tags: |-
  - youtube
permalink: bilkent-4-mart
date: 2017-02-23 22:38:00
---

4-5 Mart tarihlerinde Bilkent Universitesinde yapilacak olan [Mobil Günler](http://mobilgunler.org) konferansinda 4 Mart saat 16:00'da konusmaci olarak yer alacagim. Etkinlige cagri yapmak amaciyla [Dogukan Guven Nomak](https://twitter.com/dnomak) ve [Fatih Kadir Akin](https://twitter.com/fkadev) ile bir video cektik. Etkinlik disinda bir suru konudan da bahsettik ve ortaya cok keyifli bir video cikti. Keyifli izlemeler.

[Dogukan Guven Nomak'in YouTube Kanali](https://www.youtube.com/channel/UCbu25feEIe6fY9fZx8BCMSA)'nda sektore ve sektordeki kisilere ozel bir suru video bulabilirsiniz.

[Benim YouTube Kanalim](https://www.youtube.com/channel/UCvANtNYHe556zUWm6VzJenQ)'da ise VueJS videolari ve Frontend gelistirme sureclerine ait videolar bulabilirsiniz.

---

<iframe width="1050" height="565" src="https://www.youtube.com/embed/DQozjslLeJk" frameborder="0" allowfullscreen></iframe>
