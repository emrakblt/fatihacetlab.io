---
title: Yeni blog — Ghost
tags: |-

  - Welcome
  - Ghost Blog
permalink: yeni-blog
id: 3
updated: '2014-12-21 00:59:43'
date: 2014-08-05 17:59:40
---

Uzun zamandan beri kendi blogum ile ilgilenemiyordum. Hatta daha da kotusu epey zamandir kendi domain'im uzerinde bos bir Ghost blog duruyordu. Biseyler yazma istegim tekrar alevlendi ve Ghost blog'u biraz adam edim dedim.

#### Neden Ghost Blog?
Onceden Wordpress uzerinde kendi temami kullaniyordum, yaklasik 6 sene once yaptigim bir temaydi ve artik pek begenmiyordum. Ayrica Wordpress'in Rich Text Editor'u ile ugrasmak yerine daha basit ve kolay olan Markdown ile blog yazmak istiyordum. Karisik ayarlari olmayan sadece ve basit bir blogging platformu istiyordum. Bunun icin Ghost'u sectim.

#### Ghost uzerine bir kac soz

Ghost suan aktif olarak gelistirilmekte olan bir proje. NodeJS ile gelistirilmis ve default olarak SQLite uzerinde calisiyor. MySQL ile de kullanmak mumkun. Admin UI'i olarak guzel gorunumu var. Ghost epey minimalistik bir proje. Admin ekranlari yazi listesi, yazilari duzenleyebileceginiz bir Markdown editor ve blog adi, email adresiniz, avatariniz ve blog temasini gibi 7-8 tane basit ayarlari duzenleyebileceginiz bir ayarlar sayfasindan ibaret.

Ghost'un [hosted versiyonu](https://ghost.org/pricing/) da mevcut. Yani server, blog kurulum gibi dertleriniz olmadan 2dk icinde blog yazmaya baslayabilirsiniz. Ben hali hazirdaki Digital Ocean makineme kurmak istedim. Ghost Blog'u [kurmak](http://docs.ghost.org/installation/linux/) gercekten cok basit. NodeJS ile gelistirildigi icin `npm install --production` demek yeterli. Database olarak Ghost ile default gelen SQLite'i kullaniyorum, sanirim 50 post olana kadar sorun olmaz. Blog'u serve etmek icin nginx'i sectim. Supervisord ile surekli ayakta kalmasini, basit bir Upstart taski ile de makineyi restart ettigim zaman tekrar acilmasini sagladim. Tema olarak [su temayi](https://github.com/Bartinger/phantom) kullaniyorum. Ayrica Google Analytics, Disqus ve MailGun entegrasyonlarini yapinca hersey tamam oldu.

Kurulum icin ne yaptiginizi biliyorsaniz maksimum 2 saat icinde bos bir DigitalOcean server'ina kurulum yapabilirsiniz. Ama ugrasmak istemiyorsaniz hosted olarak kullanmayi dusunebilirsiniz.

---

Ghost ve kurulum ile ilgili bir kac link ekleyerek veda edeyim.

- [Just a Blogging Platform](https://ghost.org/about/)
- [Installing Ghost & Getting Started](http://docs.ghost.org/installation/)
- [How To Host Ghost with Nginx on DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-host-ghost-with-nginx-on-digitalocean)
- [Ghost for beginners](http://www.ghostforbeginners.com/)
