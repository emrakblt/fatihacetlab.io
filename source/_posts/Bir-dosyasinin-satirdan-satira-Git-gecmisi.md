---
title: Bir dosyasinin satirdan satira Git gecmisi
tags: |-

  - git
permalink: git-satirdan-satira-history
id: 11
updated: '2015-06-29 01:06:59'
date: 2015-06-29 00:33:51
---

Git ile bir dosyanin belli bir satirlarina dokunan commit'leri listemek icin asagidaki komutu kullanabilirsiniz.


> git rev-list HEAD -- FILENAME | (while read rev; do git blame -l -L LINESTART,+LINECOUNT $rev -- FILENAME | cut -d ' ' -f 1; done; ) | awk '{ if (!h[$0]) { print $0; h[$0]=1 } }'


Asagida `package.json` dosyasinin satir 3-9 arasina dokunan commit'ler icin kullanim ornegini gorebilirsiniz.


```
[acetz] git rev-list HEAD -- package.json | (while read rev; do git blame -l -L 3,+6 $rev -- package.json | cut -d ' ' -f 1; done; ) | awk '{ if (!h[$0]) { print $0; h[$0]=1 } }'

f413fc3282a3e74e132c2a0a12e0c2df0ce5a820
^196a6b443b5f579c517038c5506c4c6370897a0
c6ee230a05122938085d93aa19c45f07ca5e23d3

```

Yukardaki commit id'lerinden birini `git show` ile beraber kullanarak o commit'deki degisikligi gorebilirsiniz.


`git show commit_id`