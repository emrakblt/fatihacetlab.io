---
title: Object Observe
tags: |-

  - Object.observe
  - ECMAScript
  - JavaScript
permalink: object-observe-2
id: 14
updated: '2014-08-09 16:23:35'
date: 2014-08-06 01:17:34
---

ECMAScript 7'nin belki de en onemli ozelligi olan `Object.observe` Chrome 36 ile resmen hayatimiza girdi ve artik object uzerinde olusan degisiklikleri native bir sekilde dinlemek ve aksiyon almak mumkun.

Aslinda bir cogumuz gundelik hayatinda bu ozelligi Angular, Backbone, Ember gibi framework'ler ile zaten kullaniyor fakat bu ozelligin JavaScript'e native olarak geliyor olmasi cok daha heyecanlandirici bir durum. Ayrica [suradaki](https://mail.mozilla.org/pipermail/es-discuss/2012-September/024978.html) Angular developer'larin yaptigi testlerde Angular ile native `Object.observe` arasinda 20x-40x performans farki olmasi da bir baska guzel haber.


Ufak bir ornek ile nasil kullanabilecegimizi gostereyim. 

```
obj = {
	name: 'Fatih',
    nickname: 'fatih',
    age: 26,
    job: 'Developer'
}

Object.observe(obj, function(change){
	console.log(change);
});
```

`obj` diye basit bir object'imiz var ve `Object.observe(obj, callbackFn)` ile bu object uzerinde olusacak degisiklikleri dinlemeye basladik. Simdi obj.nickname degerini `fatihacet` olarak degistirelim ve asagidaki console ciktisina bakalim.

`obj.nickname = 'fatihacet'`

<img src="http://take.ms/VsRV0" />

Yukaridaki degisikligi yaptigimiz zaman callback function'imiza gelen `change` object'i icinde degisiklik ile ilgili detaylar mevcut. 

- `name` alaninda degisikligin oldugu field adi
- `object` alaninda object'imizin degisiklikden sonraki hali
- `oldValue` alaninda degisiklikden onceki degeri
- `type` alaninda ise degisikligin tipi. `update` `add` ve `delete` olabiliyor. Bizim ornegimizde `update`

`Object.observe` tabi ki array'ler icin de calisiyor. Fakat cok ufak degisiklikler ile. Asagidaki console ciktisina bakalim ve sonra devam edelim.

<img src="http://take.ms/cp3Ge" />

`k.splice(1, 1, 1)` ile arrayin 1. index'inden baslayarak 1 item cikardik ve yerine `1` sayisini koyduk ve console'da log'lagimiz `change` object'inde `addedCount`, `index` ve `removed` gibi yeni key'ler gorduk. Bunlar yapilan degisiklikle ilgili detaylar. 

- `addedCount` kac tane item ekledigimiz.
- `index` array'imizin hangi index'inde degisiklik oldugu
- `removed` ise array'den cikardigimiz item'lardan olusan bir array


Bu yeni ozellik suan icin sadece Chrome 36'da bulunmakta. Butun modern browser'lar implement ettigi zaman suan kullandigimiz butun framework'lerin cok daha performansli calismasi mumkun. Ozellikle Angular, Ember gibi framework'lerin en buyuk ozelliklerinden biri olan [Two Way Data Binding](https://docs.angularjs.org/guide/databinding)'i cok kolay bir sekilde kendi uygulamamiz icin kullanabiliriz. Basit bir ornegi icin [suradaki](http://jsfiddle.net/fatihacet/3cyhy0g9/) JSFiddle'ima bakabilirsiniz.

<iframe width="100%" height="300" src="http://jsfiddle.net/fatihacet/3cyhy0g9/embedded/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

---

Konuyla ilgili daha fazla bilgi icin,

- http://www.html5rocks.com/en/tutorials/es7/observe/
- http://addyosmani.com/blog/the-future-of-data-binding-is-object-observe/
- http://amasad.me/2014/03/16/why-im-excited-about-objectobserve
- http://georgestefanis.com/blog/2014/03/25/object-observe-ES7.html

