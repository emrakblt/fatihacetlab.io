---
title: JavaScript egitim serisi ve Pro JS kitabi hakkinda
tags: |-
  - youtube
permalink: js-egitimi
date: 2017-02-23 22:40:00
---

[Dogukan Guven Nomak](https://twitter.com/dnomak) ve [Fatih Kadir Akin](https://twitter.com/fkadev) ile cektigimiz [videoda](https://www.youtube.com/watch?v=DQozjslLeJk) bahsettigim JavaScript video egitim serisi ve Nicholas Zakas'in Pro JavaScript for Web Developers kitabini hakkinda cok email, mention ve YouTube yorumlari geldi. Ben de bir video ile bunlari anlatmak istedim ve kitabin kisaca bir incelemesini yaptim.

[YouTube Kanalim](https://www.youtube.com/channel/UCvANtNYHe556zUWm6VzJenQ)'da VueJS videolari ve Frontend gelistirme sureclerine ait videolar bulabilirsiniz.

Kanalima abone olarak yeni videolardan haberdar olabilirsiniz.
<!-- more -->
<script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channelid="UCvANtNYHe556zUWm6VzJenQ" data-layout="full" data-count="default"></div>

---

<iframe width="1050" height="565" src="https://www.youtube.com/embed/Zn4hovB4nNc" frameborder="0" allowfullscreen></iframe>
