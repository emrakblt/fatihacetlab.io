---
title: JavaScript Debugging ile ilgili bir kac onemli nokta
tags: |-

  - JavaScript
  - debugging
  - tips
permalink: javascript-debugging-tips
id: 10
updated: '2015-03-19 17:26:17'
date: 2015-03-19 17:14:00
---

Bu yazida JavaScript debugging icin onemli olan bir kac puf noktadan bahsedecegim. Bir cok kisinin bildigi bir konu ama bilmeyenlerin gunluk hayatlarini kolaylastiracagini dusundugum icin yazmak istedim. Kisa bir yazi olacak ve ekran goruntuleri Google Chrome'un Developer Tools'undan olacak.


#### `debugger` kullanimi

`debugger` kodumuzun icinde herhangi bir yerde programimizin akisinin durdurmak icin kullanabilecegimiz oldukca yararli bir arkadasimizdir. Ozellikle server tarafindan client'a gonderilen script tag'lari icine yazarsaniz o kodu bulmak icin dakikalarca aramak zorunda kalmazsiniz. Benim en cok kullandigim durum ise ornegin bir buton click'inde calistirilan fonksiyonu DevTools'dan bulmak yerine koddaki fonksiyon icerisine `debugger` yazip sayfayi yenilemek ve butona tiklamak oluyor. Cok severim kendisini <3

![http://take.ms/9R9Q8](http://take.ms/9R9Q8)


### Continue to here

debugger yada breakpoint ile kod akisini durdurduktan sonra kodumuzun bir sonraki emin oldugumuz yere kadar ilerlemesini saglamak icin kullanabiliriz. Uzun bir method'u debug ediyorsaniz ve bir akisin en basindan sonuna kadar gitmeniz gerekiyorsa aradaki adimlari atlamak icin kullanabilirsiniz.

![http://take.ms/Y8BwA](http://take.ms/Y8BwA)


### Conditional breakpoint

Breakpoint'imizin sadece belli bir durumda durmasini istiyorsak kullanabiliriz. Ozellikle bir bug'i fix ederken cok ise yariyor. Diyelim ki for ile bir data icinde donuyorsunuz. 25 tane item'iniz var ama icinden bir tanesi digerleri ile ayni formatta degil. Hata olan satira breakpoint koydugunuz zaman for icinde oldugunuz icin 25 defa ayni yerde durmaniz soz konusu olabilir ama bu sekilde breakpoint'inize bir sart ekleyip "Play" dediginiz zaman sadece sizin sartinizin saglandigi durumda kodun durmasini saglayabilirsiniz.

![http://take.ms/h1xUk](http://take.ms/h1xUk)


<small>Ekran goruntulerini TODOMVC Backbone orneginden aldim.</small>