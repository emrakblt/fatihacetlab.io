---
title: uzaktancalismak.com
tags: |-

  - remote calismak
  - remote
  - uzaktan calismak
  - remotecalismak.com
  - uzaktancalismak
  - uzaktancalismak.com
permalink: remotecalismak-com
id: 18
updated: '2017-01-17 23:48:32'
date: 2016-11-13 01:34:15
---

[uzaktancalismak.com](http://uzaktancalismak.com)

Uzun zamandan beri hayata gecirmeyi dusundugum remote calismak ile ilgili icerik sitesini sonunda tamamlayabildim. Ozellikle GitLab'e girdikten sonra hemen hemen her gun remote calismak nasil oluyor, zorluklari neler, avantajlari neler gibi sorulardan olusan mailler aliyordum. Bu yuzden bu sorulara cevap olabilecek bir ortami olusturdum ve daha cok insana ulasmasi icin [uzaktancalismak.com](http://uzaktancalismak.com) adi altinda yayinladim.

Sitede ayrica remote calismak ile ilgili ipuclari, is bulabileceginiz ortamlar ve diger insanlarin remote calismak ile ilgili yazdigi blog post'larin linkleri mevcut. Ilerleyen zamanlarda siteye

- Daha fazla Turkce blog post eklemek
- Istanbul, Izmir, Ankara gibi sehirlerde yer alan co-working space'lerini listelemek
- Remote calismayi verimli kilan araclari listelemek
- Remote calisan insanlari tanistirmak ve iletisime gecirmek
- Remote calisan insanlar ile soylesiler ve masa duzenleri hakkinda sohbetler

gibi ozellikler eklemeyi dusunuyorum. 


uzaktancalismak.com [GitHub](https://github.com/fatihacet/uzaktancalismak-com) ve [GitLab](https://gitlab.com/fatihacet/uzaktancalismak-com) üzerinde tamamen açık kaynaklıdır. Eğer eklemek, değiştirmek ya da düzeltmek istediğiniz yerler varsa GitHub/GitLab üzerinden merge request gönderebilirsiniz veya `uzaktancalismak@fatihacet.com`'a mail atabilirsiniz.


Elinizden geldigince paylasirsaniz memnun olurum.