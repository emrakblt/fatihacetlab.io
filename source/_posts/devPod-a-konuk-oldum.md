---
title: devPod'a konuk oldum
tags: |-

  - devPod
permalink: devpoda-konuk-oldum
id: 16
updated: '2016-07-11 18:04:27'
date: 2016-07-11 18:03:46
---

[devPod](http://devpod.org/)'un bu seneki tatilden onceki son bolumune yani sezon finaline konuk oldum. [Ugur Ozyilmazel aka Vigo](https://twitter.com/vigobronx) ve [Ustun Ozgur](https://twitter.com/ustunozgur) ile yaklasik bir bucuk saat Koding, GitLab ve Frontend uzerine muhabbet ettik. Bu guzel sohbet icin [devPod](http://devpod.org/) ekibine tesekkur ederim.

<iframe width="700" height="400" src="https://www.youtube.com/embed/d0b1SjKOOzI" frameborder="0" allowfullscreen></iframe>

<blockquote class="twitter-tweet" data-lang="en"><p lang="tr" dir="ltr">devPod ekibi ile cok guzel bir soylesi yaptik. Koding&#39;den, GitLab&#39;den, Frontend&#39;den bahsettik. Izleyiniz.. <a href="https://t.co/l5MswieP0d">https://t.co/l5MswieP0d</a></p>&mdash; Fatih Acet (@fatihacet) <a href="https://twitter.com/fatihacet/status/743045260700463104">June 15, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>