---
title: "Yeni Girisimim: kodilan.com"
tags: |-
  - girisim
  - kodilan
permalink: kodilan-com
date: 2019-03-13 00:05:00
---

[Emir Karsiyakali](https://twitter.com/emirkarsiyakali) ile yaptigimiz yazilim sektorune ait ilanlarin bir arada ve ulasilabilir olmasini arzuladigimiz yeni projemiz [kodilan.com](https://kodilan.com) yayinda!

---
<p style="text-align:center; margin: 40px 0;">
  <img src="/images/kodilan-logo.png" />
</p>

Yazilim sektorune ozel guncel ilanlarin bir arada ve ulasilabilir olmasini istedigimiz icin [Emir Karsiyakali](https://twitter.com/emirkarsiyakali) ile beraber [kodilan.com](https://kodilan.com) adini verdigimiz yeni bir girisime basladik.

KariyerNet, Linkedin gibi buyuk oyuncularin oldugu bu alanda asil amacimiz sadece yazilim sektorune hizmet etmek, ilanlarin guncel ve en onemlisi kaliteli olmasindan dolayi projemiz sadece uc hafta yayinda olmasina ragmen yuzun uzerinde ilan sistemimize eklendi. Ilan yayinlayanlardan aldigimiz geri bildirimler ve sitemizin almis oldugu trafik ise son derece sevindirici.

Eger [kodilan.com](https://kodilan.com)'dan haberdar olmak istiyorsaniz [Twitter](https://twitter.com/kodilancom)'da takip edebilirsiniz.

<a href="https://twitter.com/kodilancom?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-size="large" data-show-count="false">Follow @kodilancom</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<a class="twitter-timeline" href="https://twitter.com/kodilancom?ref_src=twsrc%5Etfw">Tweets by kodilancom</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
