---
title: Genclere ve kendini gelistirmek isteyenlere tavsiyeler
tags: |-
  - youtube
permalink: genclere-tavsiyeler
date: 2017-02-23 22:45:00
---

YouTube kanalimda yayinladigim bu videoda yazilim sektorune yeni giris yapmis ya da sektorde var olup kendini gelistirmek isteyenlere nacizane tavsiyelerimi aktardim. Videoya asagidan ulasabilirsiniz.

[YouTube Kanalim](https://www.youtube.com/channel/UCvANtNYHe556zUWm6VzJenQ)'da VueJS videolari ve Frontend gelistirme sureclerine ait videolar bulabilirsiniz.

Kanalima abone olarak yeni videolardan haberdar olabilirsiniz.
<!-- more -->
<script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channelid="UCvANtNYHe556zUWm6VzJenQ" data-layout="full" data-count="default"></div>

---

<iframe width="1050" height="565" src="https://www.youtube.com/embed/Ca35wp7W_jA" frameborder="0" allowfullscreen></iframe>
